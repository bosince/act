$(function () {

        // var arrDayActs  = [1200,888,800,131,1500,841,544,1561,465,481,2689,1546,151,897,568,666,789,1562,120,873,154,1445,315,784,2689,1456,1262,2654,125,3265,452,652,156,354,1235,456,652,352,1235,425,256,1240,650,451,2000,562,456,995];
        var arrDayActs  = [1200,888,800,131,1500,841,544,1561,465,481,2689,1546,151,897,568,666,789,1562,120,873,154,1445,315,784,2689,1456,1262,2654,125,3265,452,652,7356,354,95,356,125,456,454,21,452,652,65,456,325,65,189,54];

        var arrHourActs = [0.04,0.01,0.02,0.02,0.09,0.06,0.03,0.08,0.04,0.1,0.02,0.05,0.01,0.03,0.02,0,0,0.1,0.05,0.03,0.07,0.07,0.04,0.02];



        // console.log(arrDayActs.length);
        // console.log(arrHourActs.length);


        var totalCount = 450000000;

        // 获取当前时间
        var nowTime = new Date();
        // 获取开始时间
        var startTime = new Date("2018-7-15");
        // 获取结束时间
        var endTime = new Date("2018-8-31 23:59:59");

        // 获取总共时间
        var totalTime = endTime.getTime() - startTime.getTime();
        // 获取总共天数
        var totalDays=Math.ceil(totalTime/(24*3600*1000));
        // 获取总共小时数
        var totalHours=Math.ceil(totalTime/(3600*1000));

        // 获取消耗时间
        var spendTime = nowTime.getTime() - startTime.getTime();
        // 获取消耗天数
        var spendDays=Math.floor(spendTime/(24*3600*1000));
        // 获取消耗小时数
        var spendHours=Math.floor(spendTime/(3600*1000));

        console.log(spendDays + '天   ' + spendHours%24 + '小时');

        // 当天应该的总量
        var todayMustActs = arrDayActs[spendDays];
        // 截止到昨天，共成交的数量
        var beforeYesterdayActs = 0;
        // 今天实际成交的比例
        var todayActsPercentage = 0;
        
        
        for (var i = 0; i < spendDays; i++) {
            beforeYesterdayActs += arrDayActs[i];
        }
        for (var i = 0; i < spendHours%24; i++) {
            todayActsPercentage += arrHourActs[i];
        }

        // 今天实际成交的数量
        var todayActs = todayMustActs*todayActsPercentage;




        // 当前筹集量
        // var nowATC = Math.floor((beforeYesterdayActs+todayActs)*10000);
        var nowATC = 450000000;
        // 当前筹集比例
        // var nowPercentage = Math.floor((beforeYesterdayActs+todayActs)/450) + '%';
        var nowPercentage = '100%';




        $(".act-progress")
            .find(".act-progress-bar").html(nowPercentage).css({width: nowPercentage})
            .end().find(".act-progress-now").css({left: 260}).find("span").html(separation(nowATC));


        // 定义千位分隔符函数
        function separation(num){
                
            var numpart=String(num).split(".");
            numpart[0]=numpart[0].replace(new RegExp('(\\d)(?=(\\d{3})+$)','ig'),"$1,");
            return numpart.join(".");
        }

    })